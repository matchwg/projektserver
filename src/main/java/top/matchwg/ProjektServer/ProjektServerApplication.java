package top.matchwg.ProjektServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class ProjektServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektServerApplication.class, args);
	}
	
	@GetMapping("/")
	public static String nogui(@RequestParam(value = "nick", defaultValue = "Anymous") String name ) {
		return "Bye,"+name+"!";
	}
}
